<?php

namespace App\Http\Controllers;

use App\Jobs\SendEmail;
use App\Models\User;
use App\Services\Notification\Constants\EmailTypes;
use App\Services\Notification\Exceptions\UserDoesNotHaveNumber;
use App\Services\Notification\Notification;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    
    /**
     * Show send email form
     */
    public function email(){
        $users = User::all();
        $email_types = EmailTypes::toString();
        return view('notifications.send-email',compact('users','email_types'));
    }
    
    public function sendEmail(Request $request){
        
        $request->validate([
            'user' => 'integer|exists:users,id',
            'email_type' => 'integer',
        ]);
        
        try{
            $mailable = EmailTypes::toMail($request->email_type);
            SendEmail::dispatch(User::find($request->user), new $mailable);
            return redirect()->back()->with('success', __('theme.email_sent_successfully'));
        }catch(\Throwable $th){
            return redirect()->back()->with('failed', __('theme.email_service_has_problem'));
        }
    }

    public function sms(){
        $users = User::all();
        return view('notifications.send-sms',compact('users'));
    }

    public function sendSms(Request $request, Notification $notification){
        $request->validate([
            'user' => 'integer | exists:users,id',
            'text' => 'string | max:256'
        ]);
        try{
            $notification->sendSms(User::find($request->user),$request->text);
            return redirect()->back()->with('success',__('theme.sms_sent_successfully'));
        }catch(UserDoesNotHaveNumber $e){
            return redirect()->back()->with('failed', __('theme.sms_service_user_does_not_have_number'));
        }
        catch(\Throwable $th){
            return redirect()->back()->with('failed', __('theme.sms_service_has_problem'));
        }
    }
}
