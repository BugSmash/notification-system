<?php
namespace App\Services\Notification;

use App\Services\Notification\Providers\Contracts\Provider;

class Notification{
    
    public function __call($name, $arguments)
    {
        $providerPath = __NAMESPACE__."\Providers\\".substr($name,4)."Provider";
        if(!class_exists($providerPath)){
            throw new \Exception('Class is not exist!');
        }
        $providerInstance = new $providerPath(... $arguments);
        if(!is_subclass_of($providerInstance,Provider::class)){
            throw new \Exception('Class must be implements App\Services\Notification\Providers\Contracts\Provider');
        }
        $providerInstance->send();
    }
}

?>