<?php
namespace App\Services\Notification\Providers;

use App\Models\User;
use App\Services\Notification\Exceptions\UserDoesNotHaveNumber;
use App\Services\Notification\Providers\Contracts\Provider;
use GuzzleHttp\Client;


class SmsProvider implements Provider{
    
    private $user;
    private $text;

    public function __construct(User $user, String $text)
    {
        $this->user = $user;
        $this->text = $text;
    }

    public function send(){
        $this->havePhoneNumber();
        $client = new Client();
        
        $response = $client->post(config('services.sms.uri'), $this->preprationDataForSms());
        return $response->getBody();
    }

    private function preprationDataForSms(){
        $data=[
            'op' => 'send',
            'message' => $this->text,
            'to' => json_encode([$this->user->phone_number]),
        ];
        
        return [
            // multipart
            'form_params' => array_merge($data, config('services.sms.auth'))
        ];
    }

    private function havePhoneNumber(){
        if(is_null($this->user->phone_number)){
            throw new UserDoesNotHaveNumber();
        }
    }

}