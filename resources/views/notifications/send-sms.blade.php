@extends('layouts.layout')

@section('title','Email Send')
    
@section('content')
<div class="container" style="direction: rtl; text-align: right;">
    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <div class="card" style="margin-top: 25px;">
                <div class="card-header">
                    <h3 style="font-size:16px; text-align: right;">@lang('theme.sms_send')</h3>
                </div>
                <div class="card-body">
                    @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{session('success')}}
                    </div>
                    @endif
                    @if (session('failed'))
                    <div class="alert alert-danger" role="alert">
                        {{session('failed')}}
                    </div>
                    @endif
                    <form style="direction: rtl; text-align: right;" action="{{route('notification.send.sms')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="user">@lang('theme.users')</label>
                            <select class="form-control" id="user" name="user">
                            @foreach ($users as $user)
                                <option {{old('user') == $user->id ? 'selected':''}} value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="text">@lang('theme.sms_text')</label>
                            <textarea class="form-control" id="text" name="text" rows="3">{{old('text')}}</textarea>
                        </div>
                        @if ($errors->any())
                            <ul>
                                @foreach ($errors->all() as $error)
                                <div class="small mb-2">
                                    <li class="text-danger">{{$error}}</li>
                                </div>  
                                @endforeach  
                            </ul>                            
                        @endif
                        <button type="submit" class="btn btn-primary">@lang('theme.submit_btn')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection