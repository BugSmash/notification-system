<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

        <!-- Theme CSS -->
        <link href="{{ asset('css/landing-page.min.css') }}" rel="stylesheet">

        <!-- Fonts -->
        <link href="{{ asset('css/fontawesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/irsans.css') }}" rel="stylesheet">

    </head>
    <body>
        <!-- Navigation -->
        <nav class="navbar navbar-light bg-light static-top">
            <div class="container">
            <a class="navbar-brand" href="#">@lang('theme.site_name')</a>
            <a class="btn btn-primary" href="#">@lang('theme.login')</a>
            <a class="btn btn-primary" href="{{ route('notification.form.email') }}">@lang('theme.email_send')</a>
            <a class="btn btn-primary" href="{{ route('notification.form.sms') }}">@lang('theme.sms_send')</a>
            </div>
        </nav>
        @yield('content')
    </body>
</html>