<?php
return[
    'integer' => ':attribute باید به صورت عددی باشد',
    'exists' => ':attribute باید به صورت صحیح وارد شود',
    'string' => ':attribute باید صحیح وارد شود',
    'max' => [
        'string' => ':attribute بیشتر از حد مجاز :max کاراکتر است',
    ],

    'attributes' => [
        'email_type' => 'نوع ایمیل',
        'user' => 'نام کاربری',
        'text' => 'متن پیام',
    ],
];
?>