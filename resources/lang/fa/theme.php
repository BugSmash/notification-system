<?php
return[
    'site_name' => 'وب ویت',
    'login' => 'لاگین',
    'submit_btn' => 'ارسال',
    'email_send' => 'ارسال ایمیل',
    'email_type' => 'نوع ایمیل',
    'users'=> 'کاربران',
    'email_sent_successfully' => 'ایمیل با موفقیت ارسال شد',
    'email_service_has_problem' => 'سرویس ایمیل با مشکل مواجه شده است، لطفا دقایقی بعد سعی کنید',
    'sms_send' => 'ارسال پیامک',
    'sms_text' => 'متن پیامک',
    'sms_sent_successfully' => 'پیام با موفقیت ارسال شد',
    'sms_service_has_problem' => 'سرویس پیامک با مشکل مواجه شده است، لطفا دقایقی بعد سعی کنید',
    'sms_service_user_does_not_have_number' => 'کاربر انتخابی فاقد شماره همراه میباشد',
];

?>